Seringkali berguna untuk melihat bidang numerik bukan sebagai data mentah, tetapi sebagai kategori atau grup yang berbeda.
Anda dapat menggunakan CASE dengan WHEN, THEN, ELSE, dan END untuk menentukan bidang pengelompokan baru.

'Instruksi'
Menggunakan tabel negara, buat bidang baru AS geosize_group yang mengelompokkan negara menjadi tiga grup:

1. Jika surface_area lebih besar dari 2 juta, geosize_group adalah 'large'.
2. Jika surface_area lebih besar dari 350 ribu tetapi tidak lebih dari 2 juta, geosize_group adalah 'medium'.
3. Jika tidak, geosize_group adalah 'kecil'.

Solusi:

SELECT name, continent, code, surface_area,
    -- 1. First case
    CASE WHEN surface_area > 2000000 THEN 'large'
        -- 2. Second case
        WHEN surface_area > 350000 THEN 'medium'
        -- 3. Else clause + end
        ELSE 'small' END
        -- 4. Alias name
        AS geosize_group
-- 5. From table
FROM countries;
